package myregiovinco;

import audio_manager.AudioManager;
import java.io.File;
import java.io.PrintWriter;
import java.util.Optional;
import java.util.Scanner;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import pacg.PointAndClickGame;
import static myregiovinco.MyRegioVinco.*;

/**
 * This class is a concrete PointAndClickGame, as specified in The PACG
 * Framework. Note that this one plays Regio Vinco.
 * 
 * @author Richard McKenna
 * @coauthor Douglas Bell
 */
public class MyRegioVincoGame extends PointAndClickGame {
    
    // THIS PROVIDES GAME AND GUI EVENT RESPONSES
    MyRegioVincoController controller;

    // THIS PROVIDES MUSIC AND SOUND EFFECTS
    AudioManager audio;
    
    // THESE ARE THE GUI LAYERS
    Pane backgroundLayer;
    Pane gameLayer;
    Pane guiLayer;
    Pane splashLayer;
    Pane settingsScreen;
    Pane helpScreen;
    ScrollPane helpText;
    
    //TITLE ABOVE THE LOADED MAP
    Label currentTitle;
    
    //HBOX FOR ANCESTOR NAVIGATION
    HBox history;
    
    //HOVERED STATS DISPLAY
    VBox hoverStats;
    Label hoveredRegion;
    HBox numSubregions;
    Label numSubregionsLabel;
    Label numSubregionsStats;
    HBox highScore;
    Label highScoreLabel;
    Label highScoreStats;
    HBox fastestTime;
    Label fastestTimeLabel;
    Label fastestTimeStats;
    HBox fewestGuess;
    Label fewestGuessLabel;
    Label fewestGuessStats;
    
    Label mapNotPlayable;
    
    //CURRENTLY LOADED STATS DISPLAY
    HBox currentStats;
    Label currentHighScoreLabel;
    Label currentHighScoreStats;
    Label currentFastestTimeLabel;
    Label currentFastestTimeStats;
    Label currentFewestGuessesLabel;
    Label currentFewestGuessesStats;

    //variables for the stat display
    Label timer;
    Label regionsFound;
    Label regionsLeft;
    Label incorrectGuess;
    HBox stats;
    HBox stats2;
    
    //variables for the victory stats display
    Label regionV;
    Label scoreV;
    Label timerV;
    Label subRegionsV;
    Label incorrectGuessV;
    VBox statsV;
    
    //help text Text
    Label helpTextLabel;
    
    //settings buttons
    //boolean musicPressed = false;
    //boolean effectsPressed = false;
    ImageView musicView;
    ImageView effectsView;
    Button musicMute;
    Button effectsMute;
    Image unclickedButton;
    Image clickedButton;
    VBox settingsWindow;
    HBox settingsMusic;
    HBox settingsEffect;
    Label musicLabel;
    Label effectsLabel;
    
    long timeStart;
    long timePassed;
    long finalTime;
    String finalTimeS;
    int finalScore = -1;
    
    boolean regionDisabled;
    boolean capitalDisabled;
    boolean leaderDisabled;
    boolean flagDisabled;
    boolean stopDisabled;
    
    Alert stopAlert;
    ButtonType alertYes;
    ButtonType alertNo;
    Optional<ButtonType> stopAlertResult;
    
    /**
     * Get the game setup.
     */
    public MyRegioVincoGame(Stage initWindow) {
	super(initWindow, APP_TITLE, TARGET_FRAME_RATE);
	initAudio();
    }
    
    public AudioManager getAudio() {
	return audio;
    }
    
    public Pane getGameLayer() {
	return gameLayer;
    }

    /**
     * Initializes audio for the game.
     */
    private void initAudio() {
	audio = new AudioManager();
	try {
	    audio.loadAudio(TRACKED_SONG, TRACKED_FILE_NAME);
	    audio.play(TRACKED_SONG, true);
	    audio.loadAudio(SUCCESS, SUCCESS_FILE_NAME);
	    audio.loadAudio(FAILURE, FAILURE_FILE_NAME);
            audio.loadAudio(VICTORY, GENERIC_VICTORY_FILE_NAME);
	} catch (Exception e) {
	    
	}
    }
    
    // OVERRIDDEN METHODS - REGIO VINCO IMPLEMENTATIONS
    // initData
    // initGUIControls
    // initGUIHandlers
    // reset
    // updateGUI
    /**
     * Initializes the complete data model for this application, forcing the
     * setting of all game data, including all needed SpriteType objects.
     */
    @Override
    public void initData() {
	// INIT OUR DATA MANAGER
	data = new MyRegioVincoDataModel();
	data.setGameDimensions(GAME_WIDTH, GAME_HEIGHT);

	boundaryLeft = 0;
	boundaryRight = GAME_WIDTH;
	boundaryTop = 0;
	boundaryBottom = GAME_HEIGHT;
    }
    
    @Override
    public void initGUIControls() {
	// LOAD THE GUI IMAGES, WHICH INCLUDES THE BUTTONS
	backgroundLayer = new Pane();
	addStackPaneLayer(backgroundLayer);
	addGUIImage(backgroundLayer, BACKGROUND_TYPE, loadImage(BACKGROUND_FILE_PATH), BACKGROUND_X, BACKGROUND_Y);
	
	// THEN THE GAME LAYER
	gameLayer = new Pane();
	addStackPaneLayer(gameLayer);
        gameLayer.setVisible(false);
	
	// THEN THE GUI LAYER
	guiLayer = new Pane();
        guiLayer.getStylesheets().add(CSS_FILE);
        guiLayer.applyCss();
	addStackPaneLayer(guiLayer);
        
	addGUIImage(guiLayer, TITLE_TYPE, loadImage(TITLE_FILE_PATH), TITLE_X, TITLE_Y);
        addGUIButton(guiLayer, SETTINGS_BUTTON_TYPE, loadImage(SETTINGS_BUTTON_FILE_PATH), SETTINGS_BUTTON_X, SETTINGS_BUTTON_Y);
        guiButtons.get(SETTINGS_BUTTON_TYPE).setPrefSize(35,35);
        guiButtons.get(SETTINGS_BUTTON_TYPE).setTooltip(new Tooltip("Settings"));
	addGUIButton(guiLayer, HELP_BUTTON_TYPE, loadImage(HELP_BUTTON_FILE_PATH), HELP_BUTTON_X, HELP_BUTTON_Y);
        guiButtons.get(HELP_BUTTON_TYPE).setPrefSize(35,35);
        guiButtons.get(HELP_BUTTON_TYPE).setTooltip(new Tooltip("Help"));
        addGUIButton(guiLayer, GAME_BUTTON1_TYPE, loadImage(GAME_BUTTON1_PATH), GBUTTON1_X, GBUTTON1_Y);
        addGUIButton(guiLayer, GAME_BUTTON2_TYPE, loadImage(GAME_BUTTON2_PATH), GBUTTON2_X, GBUTTON2_Y);
        addGUIButton(guiLayer, GAME_BUTTON3_TYPE, loadImage(GAME_BUTTON3_PATH), GBUTTON3_X, GBUTTON3_Y);
        addGUIButton(guiLayer, GAME_BUTTON4_TYPE, loadImage(GAME_BUTTON4_PATH), GBUTTON4_X, GBUTTON4_Y);
        addGUIButton(guiLayer, GAME_BUTTON5_TYPE, loadImage(GAME_BUTTON5_PATH), GBUTTON5_X, GBUTTON5_Y);
        guiButtons.get(GAME_BUTTON1_TYPE).setTooltip(new Tooltip("Region Name Mode"));
        guiButtons.get(GAME_BUTTON2_TYPE).setTooltip(new Tooltip("Region Capital Mode"));
        guiButtons.get(GAME_BUTTON3_TYPE).setTooltip(new Tooltip("Region Leader Mode"));
        guiButtons.get(GAME_BUTTON4_TYPE).setTooltip(new Tooltip("Region Flag Mode"));
        guiButtons.get(GAME_BUTTON5_TYPE).setTooltip(new Tooltip("Stop Current Game"));
        disableRegionMode();
        disableCapitalMode();
        disableLeaderMode();
        disableFlagMode();
        disableStopButton();
        
        //region title above map
        currentTitle = new Label();
        guiLayer.getChildren().add(currentTitle);
        currentTitle.translateXProperty().setValue(CURRENT_TITLE_X);
        currentTitle.translateYProperty().setValue(CURRENT_TITLE_Y);
        currentTitle.setId("titlelabel");
        
        //history hbox, starts empty
        history = new HBox();
        guiLayer.getChildren().add(history);
        history.translateXProperty().setValue(HISTORY_X);
        history.translateYProperty().setValue(HISTORY_Y);
        
        //load the flagview that will display the hovered over flags
        ImageView flagView = new ImageView();
        flagView.setX(FLAG_CONTAINER_X);
        guiImages.put(FLAG_CONTAINER_TYPE, flagView);
        guiLayer.getChildren().add(flagView);
        flagView.setId("flaglabel");
        
        
        //set up map not playable label
        mapNotPlayable = new Label(MAP_NOT_PLAYABLE);
        mapNotPlayable.setId("mapnotplayable");
        mapNotPlayable.setLayoutX(HOVER_X-30);
        mapNotPlayable.setLayoutY(HOVER_Y+140);
        guiLayer.getChildren().add(mapNotPlayable);
        hideMapNotPlayable();
        
        //set up the hover map box and make it invisible
        hoverStats = new VBox(0);
        hoverStats.setId("hoverlabel");
        
        hoveredRegion = new Label();
        
        numSubregions = new HBox();
        numSubregionsLabel = new Label(SUB_REGIONS);
        numSubregionsStats = new Label("-");
        numSubregions.getChildren().addAll(numSubregionsLabel, numSubregionsStats);
        
        highScore = new HBox();
        highScoreLabel = new Label(HIGH_SCORE);
        highScoreStats = new Label("-");
        highScore.getChildren().addAll(highScoreLabel, highScoreStats);
        
        fastestTime = new HBox();
        fastestTimeLabel = new Label(FASTEST_TIME);
        fastestTimeStats = new Label("-");
        fastestTime.getChildren().addAll(fastestTimeLabel, fastestTimeStats);
 
        fewestGuess = new HBox();
        fewestGuessLabel = new Label(FEWEST_GUESSES);
        fewestGuessStats = new Label("-");
        fewestGuess.getChildren().addAll(fewestGuessLabel, fewestGuessStats);
        
        hoverStats.getChildren().addAll(hoveredRegion, numSubregions, highScore, fastestTime, fewestGuess);
        hoverStats.setLayoutX(HOVER_X);
        hoverStats.setLayoutY(HOVER_Y);
        guiLayer.getChildren().add(hoverStats);
        hoverStats.setVisible(false);
        
        //set up the currently load map stats
        currentStats = new HBox();
        currentStats.setId("hoverlabel");
        currentStats.setLayoutX(180);
        currentStats.setLayoutY(600);
        
        currentHighScoreLabel = new Label(HIGH_SCORE);
        currentHighScoreStats = new Label(" -     ");
        currentFastestTimeLabel = new Label(FASTEST_TIME);
        currentFastestTimeStats = new Label(" -     ");
        currentFewestGuessesLabel = new Label(FEWEST_GUESSES);
        currentFewestGuessesStats = new Label(" -     ");
        
        currentStats.getChildren().addAll(currentHighScoreLabel, currentHighScoreStats, currentFastestTimeLabel, currentFastestTimeStats, currentFewestGuessesLabel, currentFewestGuessesStats);
        guiLayer.getChildren().add(currentStats);
        currentStats.setVisible(true);
        
        //stats tracking during game
        timer = new Label("Time: -");
        regionsFound = new Label("Regions Found: - ");
        regionsLeft = new Label("Regions Left: - ");
        incorrectGuess = new Label("Incorrect Guesses: - ");
        stats = new HBox(20, regionsFound, regionsLeft, incorrectGuess);
        stats.relocate(310, 600);
        stats.setId("hoverlabel");
        stats2 = new HBox(20, timer);
        stats2.relocate(100, 600);
        stats2.setId("hoverlabel");
        hideGameStats();
        guiLayer.getChildren().addAll(stats, stats2);
        
	// NOTE THAT THE MAP IS ALSO AN IMAGE, BUT
	// WE'LL LOAD THAT WHEN A GAME STARTS, SINCE
	// WE'LL BE CHANGING THE PIXELS EACH TIME
	// FOR NOW WE'LL JUST LOAD THE ImageView
	// THAT WILL STORE THAT IMAGE
	ImageView mapView = new ImageView();
        mapView.setX(MAP_X);
	mapView.setY(MAP_Y);
        guiImages.put(MAP_TYPE, mapView);
	guiLayer.getChildren().add(mapView);
        
        
        // NOW LOAD THE WIN DISPLAY, WHICH WE'LL ONLY
	// MAKE VISIBLE AND ENABLED AS NEEDED
	ImageView winView = addGUIImage(guiLayer, WIN_DISPLAY_TYPE, loadImage(WIN_DISPLAY_FILE_PATH), WIN_X, WIN_Y);
	winView.setVisible(false);
        addGUIButton(guiLayer, WIN_RETURN_TYPE, loadImage(MAP_RETURN_BUTTON_FILE_PATH), WIN_RETURN_X, WIN_RETURN_Y);
        guiButtons.get(WIN_RETURN_TYPE).setVisible(false);
        regionV = new Label("Region: -");
        regionV.setId("victorystats");
        scoreV = new Label("Score: -");
        scoreV.setId("victorystats");
        timerV = new Label("Time: -");
        timerV.setId("victorystats");
        incorrectGuessV = new Label("Incorrect Guesses: -");
        incorrectGuessV.setId("victorystats");
        statsV = new VBox(17, regionV, scoreV, timerV, incorrectGuessV);
        statsV.relocate(385, 265);
        statsV.setVisible(false);
        guiLayer.getChildren().add(statsV);
        
        // LOAD THE SETTINGS AND HELP DISPLAY, WHICH WILL BE VISIBLE WHEN NEEDED
        ImageView settingsView = addGUIImage(guiLayer, SETTINGS_DISPLAY_TYPE,loadImage(SETTINGS_DISPLAY_FILE_PATH), SETTINGS_X,SETTINGS_Y);
        settingsView.setVisible(false);
        ImageView helpView = addGUIImage(guiLayer, HELP_DISPLAY_TYPE, loadImage(HELP_DISPLAY_FILE_PATH), HELP_X, HELP_Y);
        helpView.setVisible(false);
        helpText = new ScrollPane();
        helpText.setLayoutX(386);
        helpText.setLayoutY(222);
        helpText.setPrefWidth(443);
        helpText.setPrefHeight(268);
        helpText.setMaxSize(443, 268);
        helpText.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        helpTextLabel = new Label(HELP_TEXT);
        helpTextLabel.setWrapText(true);
        helpTextLabel.setId("helptext");
        helpText.setContent(helpTextLabel);
        helpText.setFitToWidth(true);
        guiLayer.getChildren().add(helpText);
        helpText.setVisible(false);
        
        //LOAD THE BUTTONS VISIBLE ON THESE SCREENS AND MAKE THEM INVISIBLE
        addGUIButton(guiLayer, SETTINGS_BUTTON2_TYPE, loadImage(SETTINGS_BUTTON_FILE_PATH), SETTINGS_BUTTON2_X, SETTINGS_BUTTON2_Y);
        guiButtons.get(SETTINGS_BUTTON2_TYPE).setPrefSize(35,35);
	addGUIButton(guiLayer, HELP_BUTTON2_TYPE, loadImage(HELP_BUTTON_FILE_PATH), HELP_BUTTON2_X, HELP_BUTTON2_Y);
        guiButtons.get(HELP_BUTTON2_TYPE).setPrefSize(35,35);
        addGUIButton(guiLayer, MAP_RETURN_BUTTON_TYPE, loadImage(MAP_RETURN_BUTTON_FILE_PATH), MAP_RETURN_X, MAP_RETURN_Y);
        guiButtons.get(SETTINGS_BUTTON_TYPE).setPrefSize(35,35);
        guiButtons.get(SETTINGS_BUTTON2_TYPE).setVisible(false);
        guiButtons.get(HELP_BUTTON2_TYPE).setVisible(false);
        guiButtons.get(MAP_RETURN_BUTTON_TYPE).setVisible(false);
        guiButtons.get(MAP_RETURN_BUTTON_TYPE).setTooltip(new Tooltip("Return To Map"));
        
        musicLabel = new Label("     Mute Music");
        effectsLabel = new Label("     Mute Sound Effects");
        musicLabel.setId("settingslabel");
        effectsLabel.setId("settingslabel");
        
        unclickedButton = loadImage(SETTINGS_UNCLICKED_PATH);
        clickedButton = loadImage(SETTINGS_CLICKED_PATH);
        
        musicView = new ImageView(unclickedButton);
        effectsView = new ImageView(unclickedButton);
     
        musicMute = new Button();
        effectsMute = new Button();
        
        musicMute.setGraphic(musicView);
        effectsMute.setGraphic(effectsView);
        
        settingsMusic = new HBox();
        settingsMusic.getChildren().addAll(musicMute, musicLabel);
        settingsMusic.setStyle("-fx-padding: 20px;");
        
        settingsEffect = new HBox();
        settingsEffect.getChildren().addAll(effectsMute, effectsLabel);
        settingsEffect.setStyle("-fx-padding: 20px");
        
        settingsWindow = new VBox();
        settingsWindow.getChildren().addAll(settingsMusic, settingsEffect);
        settingsWindow.setLayoutX(368);
        settingsWindow.setLayoutY(245);
        settingsWindow.setStyle("-fx-padding: 30px;");

        guiLayer.getChildren().add(settingsWindow);
        settingsWindow.setVisible(false);
        
        addGUIImage(guiLayer, BLACK_BAR_TYPE, loadImage(BLACK_BAR), BB_X, BB_Y);
        hideBlackBar();
 
        stopAlert = new Alert(AlertType.CONFIRMATION);
        stopAlert.setTitle(null);
        stopAlert.setHeaderText(null);
        stopAlert.setGraphic(null);
        stopAlert.setContentText("Stop current game?");
        alertYes = new ButtonType("YES");
        alertNo = new ButtonType("NO");
        stopAlert.getButtonTypes().setAll(alertYes, alertNo);
        DialogPane dialogPane = stopAlert.getDialogPane();
        dialogPane.getStylesheets().add(CSS_FILE);
        
        
        splashLayer = new Pane();
        splashLayer.getStylesheets().add(CSS_FILE);
        addStackPaneLayer(splashLayer);
        addGUIImage(splashLayer, SPLASH_TYPE, loadImage(SPLASH_SCREEN_FILE_PATH), SPLASH_X, SPLASH_Y);
        addGUIButton(splashLayer, SPLASH_BUTTON_TYPE, loadImage(SPLASH_START_BUTTON_FILE_PATH), SPLASH_BUTTON_X, SPLASH_BUTTON_Y);  
        
    }
    
    // HELPER METHOD FOR LOADING IMAGES
    public Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }
    
    /**
     * For initializing all the button handlers for the GUI.
     * ADD THE BUTTON THINGS HERE
     */
    @Override
    public void initGUIHandlers() {
	controller = new MyRegioVincoController(this);
       
        Button splashStart = guiButtons.get(SPLASH_BUTTON_TYPE);
        splashStart.setOnAction(e -> {
            guiImages.get(SPLASH_TYPE).setVisible(false);
            splashStart.setVisible(false);
            splashLayer.setDisable(true);
            controller.processStartGameRequest();
        });
        
        Button regionMode = guiButtons.get(GAME_BUTTON1_TYPE);
        regionMode.setOnAction(e -> {
            NAV_MODE = false;
            timeStart = System.currentTimeMillis();
            resetTimeAndScore();
            controller.processRegionGameRequest();
        });
        Button capitalMode = guiButtons.get(GAME_BUTTON2_TYPE);
        capitalMode.setOnAction(e -> {
           NAV_MODE = false;
           timeStart = System.currentTimeMillis();
           resetTimeAndScore();
           controller.processCapitalGameRequest();
        });
        Button leaderMode = guiButtons.get(GAME_BUTTON3_TYPE);
        leaderMode.setOnAction(e -> {
            NAV_MODE = false;
            timeStart = System.currentTimeMillis();
            resetTimeAndScore();
            controller.processLeaderGameRequest();
        });
        Button flagMode = guiButtons.get(GAME_BUTTON4_TYPE);
        flagMode.setOnAction(e -> {
            NAV_MODE = false;
            showBlackBar();
            timeStart = System.currentTimeMillis();
            resetTimeAndScore();
            controller.processFlagGameRequest();
        });
        Button stopMode = guiButtons.get(GAME_BUTTON5_TYPE);
        stopMode.setOnAction(e -> {
            
            data.pause();
            stopAlertResult = stopAlert.showAndWait();
            
            if (stopAlertResult.get() == alertYes){
                gameLayer.setVisible(false);
                data.reset(this);
                enableHistoryBar();
                hideBlackBar();
                enableSettingsAndHelp();
                showCurrentStats();
                hideGameStats();
                playMode = 0;
                NAV_MODE = true;
            }
            
            data.unpause();
        });
        
        Button help1 = guiButtons.get(HELP_BUTTON_TYPE);
        help1.setOnAction(e -> {
            try {
                this.beginUsingData();
                guiImages.get(SETTINGS_DISPLAY_TYPE).setVisible(false);
                guiButtons.get(HELP_BUTTON2_TYPE).setVisible(false);
                guiImages.get(HELP_DISPLAY_TYPE).setVisible(true);
                helpText.setVisible(true);
                guiButtons.get(SETTINGS_BUTTON2_TYPE).setVisible(true);
                guiButtons.get(MAP_RETURN_BUTTON_TYPE).setVisible(true);
                settingsWindow.setVisible(false);
                disableSettingsAndHelp();
                captureGameModes();
                disableAllGameButtons();
            }
            finally {
                this.endUsingData();
            }
           
        });
        
        Button settings1 = guiButtons.get(SETTINGS_BUTTON_TYPE);
        settings1.setOnAction(e -> {
            try {
                this.beginUsingData();
                guiImages.get(HELP_DISPLAY_TYPE).setVisible(false);
                helpText.setVisible(false);
                guiButtons.get(SETTINGS_BUTTON2_TYPE).setVisible(false);
                guiImages.get(SETTINGS_DISPLAY_TYPE).setVisible(true);
                guiButtons.get(HELP_BUTTON2_TYPE).setVisible(true);
                guiButtons.get(MAP_RETURN_BUTTON_TYPE).setVisible(true);
                settingsWindow.setVisible(true);
                disableSettingsAndHelp();
                captureGameModes();
                disableAllGameButtons();
            }
            finally {
                this.endUsingData();
            }
        });
        
        Button help2 = guiButtons.get(HELP_BUTTON2_TYPE);
        help2.setOnAction(e -> {
            try {   
                this.beginUsingData();
                guiImages.get(SETTINGS_DISPLAY_TYPE).setVisible(false);
                guiButtons.get(HELP_BUTTON2_TYPE).setVisible(false);
                guiImages.get(HELP_DISPLAY_TYPE).setVisible(true);
                helpText.setVisible(true);
                guiButtons.get(SETTINGS_BUTTON2_TYPE).setVisible(true);
                guiButtons.get(MAP_RETURN_BUTTON_TYPE).setVisible(true);
                settingsWindow.setVisible(false);
            }
            finally {
                this.endUsingData();
            }
        });
        
        Button settings2 = guiButtons.get(SETTINGS_BUTTON2_TYPE);
        settings2.setOnAction(e -> {
            try {
                this.beginUsingData();
                guiImages.get(HELP_DISPLAY_TYPE).setVisible(false);
                helpText.setVisible(false);
                guiButtons.get(SETTINGS_BUTTON2_TYPE).setVisible(false);
                guiImages.get(SETTINGS_DISPLAY_TYPE).setVisible(true);
                guiButtons.get(HELP_BUTTON2_TYPE).setVisible(true);
                guiButtons.get(MAP_RETURN_BUTTON_TYPE).setVisible(true);
                settingsWindow.setVisible(true);
            }
            finally {
                this.endUsingData();
            }
        });
        
        Button mapReturn = guiButtons.get(MAP_RETURN_BUTTON_TYPE);
        mapReturn.setOnAction(e -> {
            try {
                this.beginUsingData();
                guiImages.get(HELP_DISPLAY_TYPE).setVisible(false);
                helpText.setVisible(false);
                guiButtons.get(SETTINGS_BUTTON2_TYPE).setVisible(false);
                guiImages.get(SETTINGS_DISPLAY_TYPE).setVisible(false);
                guiButtons.get(HELP_BUTTON2_TYPE).setVisible(false);
                guiButtons.get(MAP_RETURN_BUTTON_TYPE).setVisible(false);
                settingsWindow.setVisible(false);
                enableSettingsAndHelp();
                restoreGameModes();
            }
            finally {
                this.endUsingData();
            }
        });
        
        Button winReturn = guiButtons.get(WIN_RETURN_TYPE);
        winReturn.setOnAction(e -> {
            try {
                this.beginUsingData();
                
                audio.stop(VICTORY);
                if (!musicPressed)
                    audio.play(TRACKED_SONG, true);
                
                winReturn.setVisible(false);
                guiImages.get(WIN_DISPLAY_TYPE).setVisible(false);
                gameLayer.setVisible(false);
                statsV.setVisible(false);
                enableSettingsAndHelp();
                enableHistoryBar();
                showCurrentStats();
                guiImages.get(MAP_TYPE).setVisible(true);
                hideBlackBar();
                data.reset(this);
                data.unpause();
                playMode = 0;
                NAV_MODE = true;
            } finally {
                this.endUsingData();
            }
        });
        
        musicMute.setOnAction( e-> {
            try {
                this.beginUsingData();
                if (musicPressed) {
                   musicPressed = false;
                   musicView.setImage(unclickedButton);
                   audio.play(TRACKED_SONG, true);
                } 
                else {
                   musicPressed = true;
                   musicView.setImage(clickedButton);
                   audio.stop(TRACKED_SONG);
                   audio.stop(VICTORY);
                } 
            } finally {
                this.endUsingData();
            }
        });
        
        effectsMute.setOnAction( e-> {
            try {
                this.beginUsingData();
                if (effectsPressed) {
                   effectsPressed = false;
                   effectsView.setImage(unclickedButton);
                } 
                else {
                   effectsPressed = true;
                   effectsView.setImage(clickedButton);
                } 
            } finally {
                this.endUsingData();
            }
        });
              
	// MAKE THE CONTROLLER THE HOOK FOR KEY PRESSES
	keyController.setHook(controller);

	// SETUP MOUSE PRESSES ON THE MAP
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setOnMousePressed(e -> {
	    controller.processMapClickRequest((int) e.getX(), (int) e.getY());
	});
        
        mapView.setOnMouseMoved(e -> {
            controller.processMapHoverRequest((int) e.getX(), (int) e.getY());
        });
        
	// KILL THE APP IF THE USER CLOSES THE WINDOW
	window.setOnCloseRequest(e->{
	    controller.processExitGameRequest();
	});
        
    }
    
     /**
     * Called when a game is restarted from the beginning, it resets all game
     * data and GUI controls so that the game may start anew.
     */
    @Override
    public void reset() {
	// IF THE WIN DIALOG IS VISIBLE, MAKE IT INVISIBLE
	ImageView winView = guiImages.get(WIN_DISPLAY_TYPE);
	winView.setVisible(false);
        guiButtons.get(WIN_RETURN_TYPE).setVisible(false);
        
        ImageView mapView = guiImages.get(MAP_TYPE);
        mapView.setVisible(true);

	// AND RESET ALL GAME DATA
        currentRegion = "The World";
        currentPath = MAP_PATH + currentRegion + "/";
	data.reset(this);
        finalTimeS = null;
        finalScore = -1;
    }
    
    
     public void loadMap() {
        currentTitle.setText(currentRegion);
	Image tempMapImage = loadImage(currentPath + currentRegion + " Map.png");
	PixelReader pixelReader = tempMapImage.getPixelReader();
	WritableImage mapImage = new WritableImage(pixelReader, (int) tempMapImage.getWidth(), (int) tempMapImage.getHeight());
	ImageView mapView = guiImages.get(MAP_TYPE);
	mapView.setImage(mapImage);
	//int numSubRegions = ((MyRegioVincoDataModel) data).getRegionsFound() + ((MyRegioVincoDataModel) data).getRegionsNotFound();
	//this.boundaryTop = -(numSubRegions * 50);

	// AND GIVE THE WRITABLE MAP TO THE DATA MODEL
	((MyRegioVincoDataModel) data).setMapImage(mapImage);
        
        //REMOVE THE ORANGE
        removeOrange(mapImage);
        
        //LOAD THE NATIONAL ANTHEM IF AVAILABLE
        
        if (new File(currentPath + currentRegion +  " National Anthem.mid").exists())
        {
            try { audio.loadAudio(VICTORY, currentPath + currentRegion + " National Anthem.mid"); }
            catch (Exception e) { }
        }
        else
        {
            try 
            { audio.loadAudio(VICTORY, GENERIC_VICTORY_FILE_NAME); }
            catch (Exception e) { }
        }
            
        loadCurrentScores();
        
        //ADD ON TO THE HISTORY BAR BUT ONLY IN NAVIGATION MODE
        if (NAV_MODE)
        {
        //add the history button and its eventhandler
        Boolean addButtonCheck = true;
        
        if (!history.getChildren().isEmpty())
        {
            for (int i = 0; i < history.getChildren().size(); i++)
            {
                Button button =  (Button) history.getChildren().get(i);
                
                if (button.getText().equals(currentRegion))
                    addButtonCheck = false;
            }
        }
        
        if (addButtonCheck)
        {
        Button historyButton = new Button(currentRegion);
        history.getChildren().add(historyButton);
        historyButton.setId("historybuttons");
        historyButton.setOnAction(e -> {
            controller.processHistoryButtonRequest(historyButton.getText());
        });
        }
        Button arrowButton = new Button(" > ");
        arrowButton.setId("historybuttons");
        history.getChildren().add(arrowButton);
        }
    }
     
    /**
     * Method to remove the outer orange parts of the map.  If the color matches
     * the pixel at (0,0) it'll be replaced.
     * @param mapImage
     */
    public void removeOrange(WritableImage mapImage){
        
        PixelReader pixelReader = mapImage.getPixelReader();
        PixelWriter pixelWriter = mapImage.getPixelWriter();
        
        Color color = pixelReader.getColor(0,0);
        
        for (int x = 0; x < mapImage.getWidth(); x++)
        {
            for (int y = 0; y < mapImage.getHeight(); y++)
            {
                if (pixelReader.getColor(x, y).equals(color))
                    {
                        pixelWriter.setColor(x, y, Color.TRANSPARENT);
                    }   
            }
        }
    }
    
    /**
     * Called each frame, this method updates the rendering state of all
     * relevant GUI controls, like displaying win and loss states and whether
     * certain buttons should be enabled or disabled.
     */
    int backgroundChangeCounter = 0;

    @Override
    public void updateGUI() {
	// IF THE GAME IS OVER, DISPLAY THE APPROPRIATE RESPONSE
	if (data.won()) {
            
            //CALCULATE THE GAME DURATION IN SECONDS
            finalTime = (System.currentTimeMillis() - timeStart)/1000;
            
            if (finalTimeS == null)
                finalTimeS = ((MyRegioVincoDataModel)data).getSecondsAsTimeText(finalTime);
            
            if (finalScore == -1)
                finalScore = (int)((1000-finalTime) - (10 * incorrectGuesses));
            
            if (finalScore < 0)
                finalScore = 0;

	    ImageView winImage = guiImages.get(WIN_DISPLAY_TYPE);
            ImageView mapView = guiImages.get(MAP_TYPE);
            mapView.setVisible(false);
	    winImage.setVisible(true); 
            statsV.setVisible(true);
            guiButtons.get(WIN_RETURN_TYPE).setVisible(true);
            disableAllGameButtons();
            hideGameStats();
            history.setDisable(true);
            guiButtons.get(HELP_BUTTON_TYPE).setDisable(true);
            guiButtons.get(SETTINGS_BUTTON_TYPE).setDisable(true);
            
            
            regionV.setText("Region: " + currentRegion);
            scoreV.setText("Score: " + finalScore);
            timerV.setText("Game Duration: " + finalTimeS);
            incorrectGuessV.setText("Incorrect Guesses: " + incorrectGuesses);
            
            saveScores(finalTime, finalScore, incorrectGuesses);
            
            data.pause();  
	}
        //update the stats on the bottom every time this gets called!
        timePassed = (System.currentTimeMillis() - timeStart)/1000;
        timer.setText("Elapsed Time: " + ((MyRegioVincoDataModel)data).getSecondsAsTimeText(timePassed));
        regionsFound.setText("Regions Found: " + ((MyRegioVincoDataModel)data).getRegionsFound());
        regionsLeft.setText("Regions Left: " + ((MyRegioVincoDataModel)data).getRegionsNotFound());
        incorrectGuess.setText("Incorrect Guesses: " + incorrectGuesses);

    }
    
    public void respondToHistoryButton(String regionName) {
        if (!regionName.equals(currentRegion))
        {
            currentPath = MAP_PATH;
            for (int i = 0; i < history.getChildren().size(); i++)
            {
                Button button = (Button) history.getChildren().get(i);
                String buttonRegion = button.getText();
                
                if (buttonRegion.equals(regionName))
                {
                    currentPath = currentPath + buttonRegion + "/";
                    currentRegion = regionName;
                    history.getChildren().remove(i+1, history.getChildren().size());
                    ((MyRegioVincoDataModel) data).reset(this);
                    return;
                }
                else
                {
                    if (!buttonRegion.equals(" > "))
                      currentPath = currentPath + buttonRegion + "/";  
                }             
            }    
        }
    }
    
    public void enableRegionMode(){
        guiButtons.get(GAME_BUTTON1_TYPE).setOpacity(1);
        guiButtons.get(GAME_BUTTON1_TYPE).setDisable(false);
    }
    public void disableRegionMode(){
        guiButtons.get(GAME_BUTTON1_TYPE).setOpacity(0.3);
        guiButtons.get(GAME_BUTTON1_TYPE).setDisable(true);
    }
    public void enableCapitalMode(){
        guiButtons.get(GAME_BUTTON2_TYPE).setOpacity(1);
        guiButtons.get(GAME_BUTTON2_TYPE).setDisable(false);
    }
    public void disableCapitalMode(){
        guiButtons.get(GAME_BUTTON2_TYPE).setOpacity(0.3);
        guiButtons.get(GAME_BUTTON2_TYPE).setDisable(true);
    }
    public void enableLeaderMode(){
        guiButtons.get(GAME_BUTTON3_TYPE).setOpacity(1);
        guiButtons.get(GAME_BUTTON3_TYPE).setDisable(false);
    }
    public void disableLeaderMode(){
        guiButtons.get(GAME_BUTTON3_TYPE).setOpacity(0.3);
        guiButtons.get(GAME_BUTTON3_TYPE).setDisable(true);
    }
    public void enableFlagMode(){
        guiButtons.get(GAME_BUTTON4_TYPE).setOpacity(1);
        guiButtons.get(GAME_BUTTON4_TYPE).setDisable(false);
    }
    public void disableFlagMode(){
        guiButtons.get(GAME_BUTTON4_TYPE).setOpacity(0.3);
        guiButtons.get(GAME_BUTTON4_TYPE).setDisable(true);
    }
    public void enableStopButton(){
        guiButtons.get(GAME_BUTTON5_TYPE).setOpacity(1);
        guiButtons.get(GAME_BUTTON5_TYPE).setDisable(false);
    }
    public void disableStopButton(){
        guiButtons.get(GAME_BUTTON5_TYPE).setOpacity(0.3);
        guiButtons.get(GAME_BUTTON5_TYPE).setDisable(true);
    }
    public void disableAllGameButtons(){
        disableRegionMode();
        disableCapitalMode();
        disableLeaderMode();
        disableFlagMode();
        disableStopButton();
    }
    public void disableForGameStart(){
        disableRegionMode();
        disableCapitalMode();
        disableLeaderMode();
        disableFlagMode();
        disableSettingsAndHelp();
    }
    public void disableHistoryBar(){
        history.setDisable(true);
    }
    public void enableHistoryBar(){
        history.setDisable(false);
    }
    
    public void hideHoverStats()
    {
        hoverStats.setVisible(false);
        getGUIImages().get(FLAG_CONTAINER_TYPE).setImage(null);
    }
    
    public void showHoverStats()
    {
        hoverStats.setVisible(true);
    }
    
    public void hideMapNotPlayable()
    {
        mapNotPlayable.setVisible(false);
    }
    
    public void showMapNotPlayable()
    {
        mapNotPlayable.setVisible(true);
    }
    public void hideBlackBar()
    {
        guiImages.get(BLACK_BAR_TYPE).setVisible(false);
    }
    
    public void showBlackBar()
    {
        guiImages.get(BLACK_BAR_TYPE).setVisible(true);
    }
    
    public void hideCurrentStats()
    {
        currentStats.setVisible(false);
    }
    
    public void showCurrentStats()
    {
        currentStats.setVisible(true);
    }
    
    public void hideGameStats()
    {
        stats.setVisible(false);
        stats2.setVisible(false);
    }
    
    public void showGameStats()
    {
        stats.setVisible(true);
        stats2.setVisible(true);
    }
    
    public void disableSettingsAndHelp(){
        guiButtons.get(HELP_BUTTON_TYPE).setDisable(true);
        guiButtons.get(HELP_BUTTON_TYPE).setOpacity(0.3);
        guiButtons.get(SETTINGS_BUTTON_TYPE).setDisable(true);
        guiButtons.get(SETTINGS_BUTTON_TYPE).setOpacity(0.3);
    }
    
    public void enableSettingsAndHelp(){
        guiButtons.get(HELP_BUTTON_TYPE).setDisable(false);
        guiButtons.get(HELP_BUTTON_TYPE).setOpacity(1);
        guiButtons.get(SETTINGS_BUTTON_TYPE).setDisable(false);
        guiButtons.get(SETTINGS_BUTTON_TYPE).setOpacity(1);
    }
    
    public void resetTimeAndScore(){
        finalTimeS = null;
        finalScore = -1;
    }
    
    public void captureGameModes()
    {
        regionDisabled = guiButtons.get(GAME_BUTTON1_TYPE).isDisable();
        capitalDisabled = guiButtons.get(GAME_BUTTON2_TYPE).isDisable();
        leaderDisabled = guiButtons.get(GAME_BUTTON3_TYPE).isDisable();
        flagDisabled = guiButtons.get(GAME_BUTTON4_TYPE).isDisable();
        stopDisabled = guiButtons.get(GAME_BUTTON5_TYPE).isDisable();
        guiImages.get(MAP_TYPE).setDisable(true);
        history.setDisable(true);
    }
    
    public void restoreGameModes()
    {
        if (!regionDisabled)
            enableRegionMode();
        if (!capitalDisabled)
            enableCapitalMode();
        if (!leaderDisabled)
            enableLeaderMode();
        if (!flagDisabled)
            enableFlagMode();
        if (!stopDisabled)
            enableStopButton();
        guiImages.get(MAP_TYPE).setDisable(false);
        history.setDisable(false);
    }
    
    public void saveScores(long finalTime, int finalScore, int incorrectGuesses)
    {
        String scores = finalTime + "@" + finalScore + "@" + incorrectGuesses;
        
        if (currentStatsFileExists())
        //READ THE FILE IF IT EXISTS TO COMPARE
        {
            try
            { 
            Scanner reader  = new Scanner(new File(currentPath + currentRegion + " Stats.txt"));
            String[] readString = reader.nextLine().split("@");
            reader.close();
            
            if (finalTime < Integer.parseInt(readString[0]))
                readString[0] = String.valueOf(finalTime);
            
            if (finalScore > Integer.parseInt(readString[1]))
                readString[1] = String.valueOf(finalScore);
            
            if (incorrectGuesses < Integer.parseInt(readString[2]))
                readString[2] = String.valueOf(incorrectGuesses);
            
            String writeString = readString[0] + "@" + readString[1] + "@" + readString[2];
            
            PrintWriter writer = new PrintWriter(currentPath+ currentRegion + " Stats.txt", "UTF-8");
            writer.print(writeString);
            writer.close(); 
            
            }
            catch (Exception e) {
                System.out.println("ERROR READING SAVED STATS FILE.");
            }                  
        }
        //CREATE THE FILE AND SAVE IF IT DOESN'T
        else
        {
            try
            {
            PrintWriter writer = new PrintWriter(currentPath + currentRegion + " Stats.txt", "UTF-8");
            writer.print(scores);
            writer.close();          
            } catch (Exception e) { 
                System.out.println("ERROR CREATING AND SAVING STATS FILE");
            }
            
        }
    }
    
    public void loadCurrentScores() {
    
        if (currentStatsFileExists())
            setCurrentRegionStatsFound();
        else
            setCurrentRegionStatsNotFound(); 
    }
    
        
    public boolean currentStatsFileExists(){
        return (new File(currentPath + currentRegion + " Stats.txt").exists());
    }
   
    public void setCurrentRegionStatsNotFound() {
        currentHighScoreStats.setText(" -     ");
        currentFastestTimeStats.setText(" -     ");
        currentFewestGuessesStats.setText(" -     "); 
    }
    
    public void setCurrentRegionStatsFound()
    {
      try
        { 
            
            Scanner reader  = new Scanner(new File(currentPath + currentRegion + " Stats.txt"));
            String[] readString = reader.nextLine().split("@");
            reader.close();
            
            long time = (long) Integer.parseInt(readString[0]);
            String timeText = ((MyRegioVincoDataModel)data).getSecondsAsTimeText(time);
            
            currentFastestTimeStats.setText(" " + timeText + "     ");
            currentHighScoreStats.setText(" " + readString[1] + "     ");
            currentFewestGuessesStats.setText(" " + readString[2] + "     ");
            
        }
      catch (Exception e) {
            System.out.println("ERROR READING STATS FILE.");
            setCurrentRegionStatsNotFound();
      }
    }
    
    public void loadHoveredScores(String hoveredSubregion)
    {
        if (hoveredStatsFileExists(hoveredSubregion))
            setHoveredRegionStatsFound(hoveredSubregion);
        else
            setHoveredRegionStatsNotFound();
    }
     
    public boolean hoveredStatsFileExists(String hoveredSubRegion){
        return (new File(currentPath + hoveredSubRegion + "/" + hoveredSubRegion + " Stats.txt").exists());
    }
    
    public void setHoveredRegionStatsNotFound()
    {
        fastestTimeStats.setText("-");
        highScoreStats.setText("-");
        fewestGuessStats.setText("-");
    }
    public void setHoveredRegionStatsFound(String hoveredSubregion)
    {
        try
        { 
            Scanner reader  = new Scanner(new File(currentPath + hoveredSubregion + "/" + hoveredSubregion + " Stats.txt"));
            String[] readString = reader.nextLine().split("@");
            reader.close();
            
            long time = (long) Integer.parseInt(readString[0]);
            String timeText = ((MyRegioVincoDataModel)data).getSecondsAsTimeText(time);
            
            fastestTimeStats.setText(" " + timeText + "     ");
            highScoreStats.setText(" " + readString[1] + "     ");
            fewestGuessStats.setText(" " + readString[2] + "     ");
            
        }
      catch (Exception e) {
            System.out.println("ERROR READING HOVERED STATS FILE.");
            setHoveredRegionStatsNotFound();
      }
    }
      
}
