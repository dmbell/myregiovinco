package myregiovinco;

import audio_manager.AudioManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;
import java.util.concurrent.locks.ReentrantLock;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import pacg.PointAndClickGame;
import pacg.PointAndClickGameDataModel;
import static myregiovinco.MyRegioVinco.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import xml_utilities.XMLUtilities;

/**
 *
 * @author Richard McKenna
 * @coauthor Douglas Bell
 */
public class MyRegioVincoDataModel extends PointAndClickGameDataModel {
    // THIS IS THE MAP IMAGE THAT WE'LL USE
    private WritableImage mapImage;
    private PixelReader mapPixelReader;
    private PixelWriter mapPixelWriter;
    
    // AND OTHER GAME DATA
    private String regionName;
    private String subRegionsType;
    private HashMap<Color, String> colorToSubRegionMappings;
    private HashMap<String, Color> subRegionToColorMappings;
    private HashMap<String, String> subRegionToCapitalMappings;
    private HashMap<String, String> capitalToSubRegionMappings;
    private HashMap<String, String> subRegionToLeaderMappings;
    private HashMap<String, String> leaderToSubRegionMappings;
    private HashMap<String, ImageView> subRegionToFlagMappings;
    private HashMap<ImageView, String> flagToSubRegionMappings;
    private HashMap<String, ArrayList<int[]>> pixels;
    private LinkedList<String> redSubRegions;
    private LinkedList<MyMovableText> subRegionStack;
   
    private String rgbVal;
    private int red;
    private int green;
    private int blue;
    
    private XMLUtilities xmlUtil;
    private Document currentDoc;
    
    private boolean regionMode;
    private boolean capitalMode;
    private boolean leaderMode;
    private boolean flagMode;
    
    public MyRegioVincoDataModel() {
	// INITIALIZE OUR DATA STRUCTURES
	colorToSubRegionMappings = new HashMap();
	subRegionToColorMappings = new HashMap();
        subRegionToCapitalMappings = new HashMap();
        capitalToSubRegionMappings = new HashMap();
        subRegionToLeaderMappings = new HashMap();
        leaderToSubRegionMappings = new HashMap();
        subRegionToFlagMappings = new HashMap();
        flagToSubRegionMappings = new HashMap();
	subRegionStack = new LinkedList();
	redSubRegions = new LinkedList();
        xmlUtil = new XMLUtilities();
    }
    
    public Document loadCurrentDoc(){
        try
        {
            Document doc = xmlUtil.loadXMLDocument(currentPath + currentRegion + " Data.xml", SCHEMA_PATH);
            //System.out.println("VALID XML");
            return doc;
        }
        catch (Exception e)
        {
            //System.out.println("INVALID XML");
            return null;
        }
    }
    
    public boolean checkXML(String regionToCheck){
        try
        {
            Document doc = xmlUtil.loadXMLDocument(currentPath + regionToCheck + "/" + regionToCheck+ " Data.xml", SCHEMA_PATH);
            //System.out.println("VALID XML");
            return true;
        }
        catch (Exception e)
        {
            //System.out.println("INVALID XML");
            return false;
        }
    }
    
    public int countSubRegions(String regionToCheck){
        try
        {
          Document doc = xmlUtil.loadXMLDocument(currentPath + regionToCheck + "/" + regionToCheck+ " Data.xml", SCHEMA_PATH);
          return doc.getElementsByTagName("sub_region").getLength();
        }
        catch (Exception e)
        {
            return -1;    
        }
    }
    
    /**
     * Resets all the game data so that a brand new map may be loaded.
     *
     * @param game the Regio Vinco game in progress
     */
    @Override
    public void reset(PointAndClickGame game) {
        // LET'S CLEAR THE DATA STRUCTURES
	colorToSubRegionMappings.clear();
	subRegionToColorMappings.clear();
        subRegionToCapitalMappings.clear();
        capitalToSubRegionMappings.clear();
        subRegionToLeaderMappings.clear();
        leaderToSubRegionMappings.clear();
        subRegionToFlagMappings.clear();
        flagToSubRegionMappings.clear();
	subRegionStack.clear();
	redSubRegions.clear();
        
        // RESET GAME MODES
        regionMode = true;
        ((MyRegioVincoGame)game).disableRegionMode();
        capitalMode = true;
        ((MyRegioVincoGame)game).disableCapitalMode();
        leaderMode = true;
        ((MyRegioVincoGame)game).disableLeaderMode();
        flagMode = true;
        ((MyRegioVincoGame)game).disableFlagMode();
        //and the stop button
        ((MyRegioVincoGame)game).disableStopButton();
        
        //attempt to parse the current XML file denoted by current path/region
        currentDoc = loadCurrentDoc();
        
        //RETURN IF IT'S BAD
        if (currentDoc == null)
            return;
        
        //get a list of all subRegions
        NodeList regionList = currentDoc.getElementsByTagName("sub_region");
        
        //go through the list and initialize the mappings
        for(int i = 0; i < regionList.getLength(); i++) 
        {
            
            Element region = (Element) regionList.item(i);
            
            //get the rgb values and name for each region
            int rgb = Integer.parseInt(region.getAttribute("red"));
            String name = region.getAttribute("name");

            //make sure all the regions have names
            if (name.equals(""))
                regionMode = false;
            
            //check if capital mode is available
            String capital = region.getAttribute("capital");
            if (capital.equals(""))
                capitalMode = false;
            
            //check if leader mode is available
            String leader = region.getAttribute("leader");
            if (leader.equals(""))
                leaderMode = false; 
            
            //check for flag for each region and map if available
            if (new File(currentPath + name + "/" + name + " Flag.png").exists())
            {
                Image flag = ((MyRegioVincoGame)game).loadImage(currentPath + name + "/" + name + " Flag.png");
                ImageView flagView = new ImageView(flag);
                subRegionToFlagMappings.put(name, flagView);
                flagToSubRegionMappings.put(flagView, name);
            }
            else
                flagMode = false;
           
            //set all mappings for each subregion, even if blank
            colorToSubRegionMappings.put(makeColor(rgb, rgb, rgb), name);
            subRegionToCapitalMappings.put(name, capital);
            capitalToSubRegionMappings.put(capital, name);
            subRegionToLeaderMappings.put(name, leader);
            leaderToSubRegionMappings.put(leader, name);
        }
        
        //ENABLE ANY AVAILABE GAME MODES
        if (regionMode)
            ((MyRegioVincoGame)game).enableRegionMode();
        if (capitalMode)
            ((MyRegioVincoGame)game).enableCapitalMode();
        if (leaderMode)
            ((MyRegioVincoGame)game).enableLeaderMode();
        if(flagMode)
            ((MyRegioVincoGame)game).enableFlagMode();
        
	// RESET THE MOVABLE TEXT
	Pane gameLayer = ((MyRegioVincoGame)game).getGameLayer();
	gameLayer.getChildren().clear();
        
	for (Color c : colorToSubRegionMappings.keySet()) {
	    String subRegion = colorToSubRegionMappings.get(c);
	    subRegionToColorMappings.put(subRegion, c);
	    MyMovableText subRegionText = new MyMovableText(subRegion);
            
            //get the color information for each text box
            red = (int) (c.getRed() * 255);
            green = (int) (c.getGreen() * 255);
            blue = (int) (c.getBlue() * 255);
 
            //set the color information
            rgbVal = "-fx-background-color: rgb(" + String.valueOf(red) + ", " 
                    + String.valueOf(green) + ", " 
                    + String.valueOf(blue) + ");";
            
            //set the style for each box
            subRegionText.setStyle(rgbVal + " -fx-text-fill: darkblue; -fx-padding: 5");
            
            //set the x layout for each box
            subRegionText.setLayoutX(STACK_X);
            
            //add the stack back to the game layer
            gameLayer.getChildren().add(subRegionText);
            
            //add each box to the subRegionStack
	    subRegionStack.add(subRegionText);
	}
        
        //shuffle the subregion stack and make the first one green
	Collections.shuffle(subRegionStack);
        subRegionStack.getFirst().setStyle("-fx-background-color: limegreen; -fx-text-fill: firebrick; -fx-padding: 5");

	int y = STACK_INIT_Y;
	int yInc = STACK_INIT_Y_INC;
	// NOW FIX THEIR Y LOCATIONS
	for (MyMovableText mT : subRegionStack) {
	    int tY = y + yInc;
	    mT.setLayoutY(tY);
	    yInc -= 50;
	}
        
	// RELOAD THE MAP
	((MyRegioVincoGame) game).loadMap();
        
	// LET'S RECORD ALL THE PIXELS
	pixels = new HashMap();
	for (MyMovableText mT : subRegionStack) {
	    pixels.put(mT.getText(), new ArrayList());
	}

	for (int i = 0; i < mapImage.getWidth(); i++) {
	    for (int j = 0; j < mapImage.getHeight(); j++) {
		Color c = mapPixelReader.getColor(i, j);
		if (colorToSubRegionMappings.containsKey(c)) {
		    String subRegion = colorToSubRegionMappings.get(c);
		    ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
		    int[] pixel = new int[2];
		    pixel[0] = i;
		    pixel[1] = j;
		    subRegionPixels.add(pixel);
		}
	    }
	}
        
        //check to see what regions are available to click
        for (MyMovableText mT : subRegionStack)
        {
            String checkFolder = mT.getText();
            
            if (!new File(currentPath + checkFolder).exists())       
                changeSubRegionColorOnMap(checkFolder, INVALID_REGION_COLOR);
            else 
            {
                if (!checkXML(checkFolder))
                    changeSubRegionColorOnMap(checkFolder, INVALID_REGION_COLOR); 
            }
        }

	//RESET THE AUDIO
	AudioManager audio = ((MyRegioVincoGame) game).getAudio();
        if (!musicPressed)
        {
            audio.stop(VICTORY);
            if (!audio.isPlaying(TRACKED_SONG)) {
	    audio.play(TRACKED_SONG, true);
        }
	}
        
        
        //RESET THE INCORRECT GUESS COUNTER
        incorrectGuesses = 0;
        
	// LET'S GO
	beginGame();  
    }
    
    public String getSecondsAsTimeText(long numSeconds) {
	long numHours = numSeconds / 3600;
	numSeconds = numSeconds - (numHours * 3600);
	long numMinutes = numSeconds / 60;
	numSeconds = numSeconds - (numMinutes * 60);

	String timeText = "";
	if (numHours > 0) {
	    timeText += numHours + ":";
	}
	timeText += numMinutes + ":";
	if (numSeconds < 10) {
	    timeText += "0" + numSeconds;
	} else {
	    timeText += numSeconds;
	}
	return timeText;
    }
    
    public int getRegionsFound() {
	return colorToSubRegionMappings.keySet().size() - subRegionStack.size();
    }

    public int getRegionsNotFound() {
	return subRegionStack.size();
    }
    
    public void respondToMapSelection(MyRegioVincoGame game, int x, int y) {
    // THIS IS WHERE WE'LL CHECK TO SEE IF THE
    // PLAYER CLICKED NO THE CORRECT SUBREGION
        
        Color pixelColor = mapPixelReader.getColor(x, y);
        String clickedSubRegion = colorToSubRegionMappings.get(pixelColor);
        String clickedSubRegionDupe = colorToSubRegionMappings.get(pixelColor);;

        if ((clickedSubRegion == null) || (subRegionStack.isEmpty()))
            return;

        if (!NAV_MODE)
        {
        //capital mode
        if (playMode == 2)
             clickedSubRegion = subRegionToCapitalMappings.get(colorToSubRegionMappings.get(pixelColor));
        
        //leader mode
        if (playMode == 3)
            clickedSubRegion = subRegionToLeaderMappings.get(colorToSubRegionMappings.get(pixelColor));
        
        if (clickedSubRegion.equals(subRegionStack.get(0).getText())) {
            // YAY, CORRECT ANSWER
            if (!effectsPressed)
                game.getAudio().play(SUCCESS, false);

            // TURN THE TERRITORY GREEN
            changeSubRegionColorOnMap(clickedSubRegionDupe, Color.GREEN);

            // AND LET'S CHANGE THE RED ONES BACK TO THEIR PROPER COLORS
            for (String s : redSubRegions) {
                Color subRegionColor = subRegionToColorMappings.get(s);
                changeSubRegionColorOnMap(s, subRegionColor);
            }
            redSubRegions.clear();

            // REMOVE THE BOTTOM ELEMENT FROM THE STACK
            MyMovableText removeMe = subRegionStack.removeFirst();
            game.getGameLayer().getChildren().remove(removeMe);

            startTextStackMovingDown();

            if (subRegionStack.isEmpty()) {
                this.endGameAsWin();
                if (!musicPressed)
                {
                    game.getAudio().stop(TRACKED_SONG);
                    //LOAD PROPER VICTORY SONG IF AVAILABE
                    game.getAudio().play(VICTORY, false);
                }
            }
            else
            {
                if (playMode != 4)
                subRegionStack.getFirst().setStyle("-fx-background-color: limegreen; -fx-text-fill: firebrick; -fx-padding: 5");
            }

        } 
        else 
        {
            if (!redSubRegions.contains(clickedSubRegion)) {
                // BOO WRONG ANSWER
                incorrectGuesses++;
                if (!effectsPressed)
                    game.getAudio().play(FAILURE, false);

                // TURN THE TERRITORY TEMPORARILY RED
                changeSubRegionColorOnMap(clickedSubRegionDupe, Color.RED);
                redSubRegions.add(clickedSubRegionDupe);
            }
        }
        
        }
        //THIS IS FOR NAVIGATION MODE
        else
        {
            String oldCurrentRegion = currentRegion;
            String oldCurrentPath = currentPath;
            currentRegion = clickedSubRegion;
            currentPath = currentPath + clickedSubRegion + "/";
            Document testDoc = loadCurrentDoc();
            
            if (testDoc == null)
            {
                currentRegion = oldCurrentRegion;
                currentPath = oldCurrentPath;
            }
            else
            {
            reset(game);
            }
        }

    }
        
    public void resetColorsForPlayModes(){
        for (MyMovableText mT : subRegionStack) {
            String region = mT.getText();
            Color regionColor = subRegionToColorMappings.get(region);
            changeSubRegionColorOnMap(region, regionColor);
        }
    }
    
    public void startTextStackMovingDown() {
	// AND START THE REST MOVING DOWN
	for (MyMovableText mT : subRegionStack) 
        {
	    mT.setVelocityY(SUB_STACK_VELOCITY);
        }
    }
    
    public void respondToMapHover(MyRegioVincoGame game, int x, int y) {
        
        Color pixelColor = mapPixelReader.getColor(x, y);
        String hoveredSubRegion = colorToSubRegionMappings.get(pixelColor);
        
        if (NAV_MODE)
        {
            if (hoveredSubRegion != null)
            {
                //CHECK FOR STATS
                //SET VISIBLE IF THE CHECK PROVES TRUE
                game.hideMapNotPlayable();
                game.loadHoveredScores(hoveredSubRegion);
                game.showHoverStats();
                game.hoveredRegion.setText(hoveredSubRegion);
                
                //TRY COUNTING THE NUMBER OF SUBREGIONS
                if (countSubRegions(hoveredSubRegion) != -1)
                    game.numSubregionsStats.setText(String.valueOf(countSubRegions(hoveredSubRegion)));
                
                try
                {
                     Image flag = game.loadImage(currentPath + hoveredSubRegion + "/" + hoveredSubRegion + " Flag.png");
                     
                     game.getGUIImages().get(FLAG_CONTAINER_TYPE).setY(GAME_HEIGHT-(95+flag.getHeight()));
                     game.getGUIImages().get(FLAG_CONTAINER_TYPE).setImage(flag);
                }
                catch (Exception e)
                {
                    game.getGUIImages().get(FLAG_CONTAINER_TYPE).setImage(null);
                }
                
            }
            else if (pixelColor.equals(INVALID_REGION_COLOR))
            {
                game.hideHoverStats();
                game.showMapNotPlayable();
            }
            else
            {
                game.hideHoverStats();
                game.hideMapNotPlayable();
            }
        }
        //DON'T DO ANYTHING IF THE USER IS IN GAME MODE
        else
            return;
    }
    /**
     * Called each frame, this thread already has a lock on the data. This
     * method updates all the game data as needed.
     *
     * @param game the game in progress
     * @param percentage leave me alone
     */
    @Override
    public void updateAll(PointAndClickGame game, double percentage) {
	
        if (playMode != 4)
        {
        for (MyMovableText mT : subRegionStack) {
            mT.update(percentage);
	} 
	if (!subRegionStack.isEmpty()) {
	    MyMovableText bottomOfStack = subRegionStack.get(0);
	    double bottomY = bottomOfStack.getLayoutY() + bottomOfStack.translateYProperty().doubleValue();
	    if (bottomY >= FIRST_REGION_Y_IN_STACK) {
		double diffY = bottomY - FIRST_REGION_Y_IN_STACK;
                while ((bottomOfStack.getLayoutY() + bottomOfStack.translateYProperty().doubleValue()) != FIRST_REGION_Y_IN_STACK){
                    for (MyMovableText mT : subRegionStack) {
                        mT.setLayoutY(mT.getLayoutY() - diffY);
                        mT.setVelocityY(0);
                    }
                }
	    }
	}
        }
        else
        {
            for (MyMovableText mT : subRegionStack) {
            mT.update(percentage);
	} 
	if (!subRegionStack.isEmpty()) {
	    MyMovableText bottomOfStack = subRegionStack.get(0);
            int bottomHeight = GAME_HEIGHT-95-((int)(((ImageView)bottomOfStack.getGraphic()).getImage().getHeight()));
	    double bottomY = bottomOfStack.getLayoutY() + bottomOfStack.translateYProperty().doubleValue();
	    if (bottomY >= bottomHeight) {
		double diffY = bottomY - bottomHeight;
                while ((bottomOfStack.getLayoutY() + bottomOfStack.translateYProperty().doubleValue()) != bottomHeight){
                    for (MyMovableText mT : subRegionStack) {
                        mT.setLayoutY(mT.getLayoutY() - diffY);
                        mT.setVelocityY(0);
                    }
                }
	    }
	}
        }
        
    }
    
     /**
     * Called each frame, this method specifies what debug text to render. Note
     * that this can help with debugging because rather than use a
     * System.out.print statement that is scrolling at a fast frame rate, we can
     * observe variables on screen with the rest of the game as it's being
     * rendered.
     *
     * @return game the active game being played
     */
    public void updateDebugText(PointAndClickGame game) {
	debugText.clear();
    }

        // HELPER METHOD FOR MAKING A COLOR OBJECT
    public static Color makeColor(int r, int g, int b) {
	return Color.color(r/255.0, g/255.0, b/255.0);
    }
    
    public void setMapImage(WritableImage initMapImage) {
	mapImage = initMapImage;
	mapPixelReader = mapImage.getPixelReader();
	mapPixelWriter = mapImage.getPixelWriter();
    }
    
    public void changeSubRegionColorOnMap(String subRegion, Color color) {
        // THIS IS WHERE WE'LL CHECK TO SEE IF THE
	// PLAYER CLICKED NO THE CORRECT SUBREGION
	ArrayList<int[]> subRegionPixels = pixels.get(subRegion);
        
	for (int[] pixel : subRegionPixels) 
	    mapPixelWriter.setColor(pixel[0], pixel[1], color);
    }
    
    public void removeAllButOneFromeStack(MyRegioVincoGame game) {
        if (!redSubRegions.isEmpty())
        {
            for (String s : redSubRegions) {
                Color subRegionColor = subRegionToColorMappings.get(s);
		changeSubRegionColorOnMap(s, subRegionColor);
	    }
	    redSubRegions.clear();
        }
	while (subRegionStack.size() > 1) {
	    MyMovableText text = subRegionStack.removeFirst();
            String subRegionName = text.getText();
            
            if (playMode == 2)
                subRegionName = capitalToSubRegionMappings.get(text.getText());
            
            if (playMode == 3)
                subRegionName = leaderToSubRegionMappings.get(text.getText());
            
            game.getGameLayer().getChildren().remove(text);
            if (playMode != 4)
                subRegionStack.getFirst().setStyle("-fx-background-color: limegreen; -fx-text-fill: firebrick; -fx-padding: 5");
	    // TURN THE TERRITORY GREEN IF NECESSARY
            changeSubRegionColorOnMap(subRegionName, Color.GREEN);
	}
	startTextStackMovingDown();
    }
    
    public void setUpRegionMode(PointAndClickGame game)
    {
        playMode = 1;
        resetColorsForPlayModes();
        ((MyRegioVincoGame)game).hideHoverStats();
        ((MyRegioVincoGame)game).hideMapNotPlayable();
        ((MyRegioVincoGame)game).hideCurrentStats();
        ((MyRegioVincoGame)game).showGameStats();
        ((MyRegioVincoGame)game).disableHistoryBar();
        ((MyRegioVincoGame)game).getGameLayer().setVisible(true);
        ((MyRegioVincoGame)game).enableStopButton();
        ((MyRegioVincoGame)game).disableForGameStart();
    }
    public void setUpCapitalMode(PointAndClickGame game)
    { 
        playMode = 2;
        resetColorsForPlayModes();
        ((MyRegioVincoGame)game).hideHoverStats();
        ((MyRegioVincoGame)game).hideMapNotPlayable();
        ((MyRegioVincoGame)game).hideCurrentStats();
        ((MyRegioVincoGame)game).showGameStats();
        ((MyRegioVincoGame)game).disableHistoryBar();
        ((MyRegioVincoGame)game).getGameLayer().setVisible(true);
        ((MyRegioVincoGame)game).enableStopButton();
        ((MyRegioVincoGame)game).disableForGameStart();
        
        for (MyMovableText mT : subRegionStack)
        {
            mT.setText(subRegionToCapitalMappings.get(mT.getText()));
        }
    }
    
    public void setUpLeaderMode(PointAndClickGame game)
    {
        playMode = 3;
        resetColorsForPlayModes();
        ((MyRegioVincoGame)game).hideHoverStats();
        ((MyRegioVincoGame)game).hideMapNotPlayable();
        ((MyRegioVincoGame)game).hideCurrentStats();
        ((MyRegioVincoGame)game).showGameStats();
        ((MyRegioVincoGame)game).disableHistoryBar();
        ((MyRegioVincoGame)game).getGameLayer().setVisible(true);
        ((MyRegioVincoGame)game).enableStopButton();
        ((MyRegioVincoGame)game).disableForGameStart();
        
        for (MyMovableText mT : subRegionStack)
        {
            mT.setText(subRegionToLeaderMappings.get(mT.getText()));
        }
    }
    
    public void setUpFlagMode(PointAndClickGame game)
    {
        playMode = 4;
        resetColorsForPlayModes();
        ((MyRegioVincoGame)game).hideHoverStats();
        ((MyRegioVincoGame)game).hideMapNotPlayable();
        ((MyRegioVincoGame)game).hideCurrentStats();
        ((MyRegioVincoGame)game).showGameStats();
        ((MyRegioVincoGame)game).getGameLayer().setVisible(true);
        ((MyRegioVincoGame)game).disableHistoryBar();
        ((MyRegioVincoGame)game).enableStopButton();
        ((MyRegioVincoGame)game).disableForGameStart();
        
        //first set all the flags and delete the names
        for (MyMovableText mT: subRegionStack)
        {

            ImageView flag = subRegionToFlagMappings.get(mT.getText());
            double width = flag.getImage().getWidth();
            double height = flag.getImage().getHeight();
            mT.setGraphic(flag);  
            mT.setPrefWidth(width);
            mT.setPrefHeight(height);
            mT.autosize();
            mT.setStyle("-fx-background-color: black; -fx-padding: 5px;"
                    + "-fx-text-color: black;"); 
        }
        
        //then reposition the stack panels based on the flags
        int y = GAME_HEIGHT-95;
	int yInc = 0;
        
	// NOW FIX THEIR Y LOCATIONS
	for (MyMovableText mT : subRegionStack) {
            yInc -= ((int)(((ImageView)mT.getGraphic()).getImage().getHeight()));
	    int tY = y + yInc;
	    mT.setLayoutY(tY);         
	}          
    }
    
}
