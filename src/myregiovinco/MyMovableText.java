package myregiovinco;

import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author spandolfo
 */
public class MyMovableText extends Label {
        // USED FOR MANAGING NODE MOVEMENT
    protected double[] velocity = new double[2];
    protected double[] acceleration = new double[2];

    /**
     * Constructor for initializing a GameNode, note that the provided
     * text argument should not be null.
     * 
     * @param initText The text managed by this object.
     */
    public MyMovableText(String string){
	super(string);
        this.setPrefHeight(45);
        this.setPrefWidth(295);
        this.setFont(Font.font("Times New Roman", FontWeight.BOLD, 22));
    }
    
    // ACCESSOR AND MUTATOR METHODS
    public double getVelocityX() {
	return velocity[0];
    }
    
    public double getVelocityY() {
	return velocity[1];
    }
    
    public void setVelocityX(double initVelocityX) {
	velocity[0] = initVelocityX;
    }
    
    public void setVelocityY(double initVelocityY) {
	velocity[1] = initVelocityY;
    }
    
    public void setAccelerationX(double initAccelerationX){
        acceleration[0] = initAccelerationX;
    }
    
    public void setAccelerationY(double initAccelerationY){
        acceleration[1] = initAccelerationY;
    }

    /**
     * Called each frame, this function moves the node according
     * to its current velocity and updates the velocity according to
     * its current acceleration, applying percentage as a weighting for how
     * much to scale the velocity and acceleration this frame.
     * 
     * @param percentage The percentage of a frame this the time step
     * that called this method represents.
     */
    public void update(double percentage) {
	double x = this.translateXProperty().doubleValue();
	this.translateXProperty().setValue(x + (velocity[0] * percentage));
	double y = this.translateYProperty().doubleValue();
	this.translateYProperty().setValue(y + (velocity[1] * percentage));
	 
	// UPDATE VELOCITY
        velocity[0] += (acceleration[0] * percentage);
	velocity[1] += (acceleration[1] * percentage);
    } 
}
