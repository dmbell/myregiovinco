package myregiovinco;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import static myregiovinco.MyRegioVinco.NAV_MODE;
import pacg.KeyPressHook;

/**
 * This controller provides the appropriate responses for all interactions.
 */
public class MyRegioVincoController implements KeyPressHook {
    MyRegioVincoGame game;
    
    public MyRegioVincoController(MyRegioVincoGame initGame) {
	game = initGame;
    }
    
    public void processStartGameRequest() {
        try
        {
            game.beginUsingData();
            game.reset();
        }
        finally
        {
            game.endUsingData();
        }
    }
    
    public void processExitGameRequest() {
        try
        {
            game.beginUsingData();
            game.killApplication();
        }
	finally
        {
            game.endUsingData();
        }
    }
    
    public void processMapClickRequest(int x, int y) {
        try
        {
            game.beginUsingData();
            ((MyRegioVincoDataModel)game.getDataModel()).respondToMapSelection(game, x, y);
        }
	finally
        {
            game.endUsingData();
        }
    }
    
    public void processMapHoverRequest(int x, int y) {
        try
        {
            game.beginUsingData();
            ((MyRegioVincoDataModel)game.getDataModel()).respondToMapHover(game, x, y);
        }
	finally
        {
            game.endUsingData();
        }
    }
    
    public void processHistoryButtonRequest(String regionName) {
        try
        {
            game.beginUsingData();
            game.respondToHistoryButton(regionName);
        }
	finally
        {
            game.endUsingData();
        }
    }
    
     public void processRegionGameRequest() {
        try
        {
            game.beginUsingData();
            ((MyRegioVincoDataModel)game.getDataModel()).setUpRegionMode(game);
        }
        finally
        {
            game.endUsingData();
        }
    }
     
    public void processCapitalGameRequest() {
        try
        {
            game.beginUsingData();
            ((MyRegioVincoDataModel)game.getDataModel()).setUpCapitalMode(game);
        }
        finally
        {
            game.endUsingData();
        }
    }
    
    public void processLeaderGameRequest() {
        try
        {
            game.beginUsingData();
            ((MyRegioVincoDataModel)game.getDataModel()).setUpLeaderMode(game);
        }
        finally
        {
            game.endUsingData();
        }
    }
    
    public void processFlagGameRequest() {
        try
        {
            game.beginUsingData();
            ((MyRegioVincoDataModel)game.getDataModel()).setUpFlagMode(game);
        }
        finally
        {
            game.endUsingData();
        }
    }
    
    @Override
    public void processKeyPressHook(KeyEvent ke)
    {
        if (!NAV_MODE)
        {
        KeyCode keyCode = ke.getCode();
        if (keyCode == KeyCode.C)
        {
            try
            {    
                game.beginUsingData();
                MyRegioVincoDataModel dataModel = (MyRegioVincoDataModel)(game.getDataModel());
                dataModel.removeAllButOneFromeStack(game);         
            }
            finally
            {
                game.endUsingData();
            } 
        }
        }
    } 
    
}

