package myregiovinco;


import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * This is the core Regio vinco game application.  Note that it extends the
 * PointAndClickGame class and overrides all the proper methods for setting up
 * the Data, the GUI, the Event Handlers, and update and timer task, the thing
 * that actually does the update scheduled rendering.
 * 
 * @author Richard McKenna
 * @coauthor Douglas Bell
 */
public class MyRegioVinco extends Application {
    
    // THESE CONSTANTS SETUP THE GAME DIMENSIONS. THE GAME WIDTH
    // AND HEIGHT SHOULD MIRROR THE BACKGROUND IMAGE DIMENSIONS. WE
    // WILL NOT RENDER ANYTHING OUTSIDE THOSE BOUNDS
    public static final int GAME_WIDTH = 1200;
    public static final int GAME_HEIGHT = 700;

    //MAP PATHS
    public static final String MAP_PATH = "./data/maps/";
    public static String currentPath;
    public static String currentRegion;
    
    public static final String SCHEMA_PATH = MAP_PATH + "RegionData.xsd";
    public static final String WORLD_XML_PATH = MAP_PATH + "The World/The World Data.xml";
    
    // HERE ARE THE PATHS TO THE IMAGES WE'LL USE
    public static final String GUI_PATH = "./data/gui/";
    public static final String BACKGROUND_FILE_PATH = GUI_PATH + "RegioVincoBackground.jpg";
    public static final String TITLE_FILE_PATH = GUI_PATH + "RegioVincoTitle.png";
    public static final String SPLASH_SCREEN_FILE_PATH = GUI_PATH + "RegioVincoSplashScreen.jpg";
    public static final String SPLASH_START_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoSplashStart.png";
    public static final String SUB_REGION_FILE_PATH = GUI_PATH + "RegioVincoSubRegion.png";
    public static final String WIN_DISPLAY_FILE_PATH = GUI_PATH + "RegioVincoWinDisplay.png";
    public static final String SETTINGS_DISPLAY_FILE_PATH = GUI_PATH + "RegioVincoSettingsDisplay.png";
    public static final String HELP_DISPLAY_FILE_PATH = GUI_PATH + "RegioVincoHelpDisplay.png";
    public static final String SETTINGS_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoSettingsButton.png";
    public static final String HELP_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoHelpButton.png";
    public static final String MAP_RETURN_BUTTON_FILE_PATH = GUI_PATH + "RegioVincoReturnMapButton.png";
    public static final String GAME_BUTTON1_PATH = GUI_PATH + "GameButton1.png";
    public static final String GAME_BUTTON2_PATH = GUI_PATH + "GameButton2.png";
    public static final String GAME_BUTTON3_PATH = GUI_PATH + "GameButton3.png";
    public static final String GAME_BUTTON4_PATH = GUI_PATH + "GameButton4.png";
    public static final String GAME_BUTTON5_PATH = GUI_PATH + "GameButton5.png";
    public static final String SETTINGS_UNCLICKED_PATH = GUI_PATH + "SettingsButtonUnclicked.png";
    public static final String SETTINGS_CLICKED_PATH = GUI_PATH + "SettingsButtonClicked.png";
    public static final String BLACK_BAR = GUI_PATH + "BlackBar.png";

    // HERE ARE SOME APP-LEVEL SETTINGS, LIKE THE FRAME RATE. ALSO,
    // WE WILL BE LOADING SpriteType DATA FROM A FILE, SO THAT FILE
    // LOCATION IS PROVIDED HERE AS WELL. NOTE THAT IT MIGHT BE A 
    // GOOD IDEA TO LOAD ALL OF THESE SETTINGS FROM A FILE, BUT ALAS,
    // THERE ARE ONLY SO MANY HOURS IN A DAY
    public static final int TARGET_FRAME_RATE = 30;
    public static final String APP_TITLE = "Regio Vinco";
    
    // BACKGROUND IMAGE
    public static final String BACKGROUND_TYPE = "BACKGROUND_TYPE";
    public static final int BACKGROUND_X = 0;
    public static final int BACKGROUND_Y = 0;
    
    //MAP LOCATION
    public static final String MAP_TYPE = "MAP_TYPE";
    public static final int MAP_X = 0;
    public static final int MAP_Y = 0;
    
    //THE BLACK BAR
    public static final String BLACK_BAR_TYPE = "BLACK_BAR_TYPE";
    public static final int BB_X = 1109;
    public static final int BB_Y = 157;
    
    // TITLE IMAGE
    public static final String TITLE_TYPE = "TITLE_TYPE";
    public static final int TITLE_X = 900;
    public static final int TITLE_Y = 0;
    
    //CURRENT TITLE
    public static final int CURRENT_TITLE_X = 370;
    public static final int CURRENT_TITLE_Y = 5;
    
    //HISTORY HBOX
    public static final int HISTORY_X = 53; //heh
    public static final int HISTORY_Y = 668;
                   
    //SPLASH BUTTON
    public static final String SPLASH_BUTTON_TYPE = "SPLASH_BUTTON_TYPE";
    public static final int SPLASH_BUTTON_X = 527;
    public static final int SPLASH_BUTTON_Y = 615;
    
    //SPLASH IMAGE
    public static final String SPLASH_TYPE = "SPLASH_TYPE";
    public static final int SPLASH_X = 0;
    public static final int SPLASH_Y =0;
    
    //STATS DISPLAY
    public static final String STATS_DISPLAY_TYPE = "DISPLAY_TYPE";
    public static final int D_T_X = 10;
    public static final int D_T_Y = 650;
    public static final int HOVER_X = 927;
    public static final int HOVER_Y = 188;
    public static final String SUB_REGIONS = "Subregions: ";
    public static final String HIGH_SCORE = "High Score: ";
    public static final String FASTEST_TIME = "Fastest Time: ";
    public static final String FEWEST_GUESSES = "Fewest Guesses: ";

    // THE WIN DIALOG
    public static final String WIN_DISPLAY_TYPE = "WIN_DISPLAY";
    public static final int WIN_X = 350;
    public static final int WIN_Y = 150;
    
    // THE WIN RETURN BUTTON
    public static final String WIN_RETURN_TYPE = "WIN_RETURN";
    public static final int WIN_RETURN_X = 580;
    public static final int WIN_RETURN_Y = 500;
    
    // THE HELP DIALOG
    public static final String HELP_DISPLAY_TYPE = "HELP_DISPLAY_TYPE";
    public static final int HELP_X = 350;
    public static final int HELP_Y = 150;
    
    //THE HELP BUTTON
    public static final String HELP_BUTTON_TYPE = "HELP_BUTTON_TYPE";
    public static final int HELP_BUTTON_X = 862;
    public static final int HELP_BUTTON_Y = 7;
    
    //THE SECOND HELP BUTTON
    public static final String HELP_BUTTON2_TYPE = "HELP_BUTTON2_TYPE";
    public static final int HELP_BUTTON2_X = 562;
    public static final int HELP_BUTTON2_Y = 498;
    
    // THE SETTINGS DIALOG
    public static final String SETTINGS_DISPLAY_TYPE = "SETTINGS_DISPLAY";
    public static final int SETTINGS_X = 350;
    public static final int SETTINGS_Y = 150;
    
    // THE SETTINGS BUTTON
    public static final String SETTINGS_BUTTON_TYPE = "SETTINGS_BUTTON_TYPE";
    public static final int SETTINGS_BUTTON_X = 861;
    public static final int SETTINGS_BUTTON_Y = 53; 
    
    // THE SECOND SETTINGS BUTTON
    public static final String SETTINGS_BUTTON2_TYPE = "SETTINGS_BUTTON2_TYPE";
    public static final int SETTINGS_BUTTON2_X = 562;
    public static final int SETTINGS_BUTTON2_Y = 501;
    
    //THE MAP RETURN BUTTON
    public static final String MAP_RETURN_BUTTON_TYPE = "MAP_RETURN_BUTTON_TYPE";
    public static final int MAP_RETURN_X = 598;
    public static final int MAP_RETURN_Y = 501;
    
    //GAMEPLAY BUTTONS
    public static final String GAME_BUTTON1_TYPE = "GAME_BUTTON1_TYPE";
    public static final String GAME_BUTTON2_TYPE = "GAME_BUTTON2_TYPE";
    public static final String GAME_BUTTON3_TYPE = "GAME_BUTTON3_TYPE";
    public static final String GAME_BUTTON4_TYPE = "GAME_BUTTON4_TYPE";
    public static final String GAME_BUTTON5_TYPE = "GAME_BUTTON5_TYPE";
    public static final int GBUTTON1_X = 898;
    public static final int GBUTTON2_X = 959;
    public static final int GBUTTON3_X = 1019;
    public static final int GBUTTON4_X = 1079;
    public static final int GBUTTON5_X = 1139;
    public static final int GBUTTON1_Y = 105;
    public static final int GBUTTON2_Y = 106;
    public static final int GBUTTON3_Y = 108;
    public static final int GBUTTON4_Y = 105;
    public static final int GBUTTON5_Y = 106;
    
    //FLAG HOLDER
    public static final String FLAG_CONTAINER_TYPE = "Flag Container";
    public static final int FLAG_CONTAINER_X = 927;
    
    // THIS IS THE X WHERE WE'LL DRAW ALL THE STACK NODES
    public static final int STACK_X = 900;
    public static final int STACK_INIT_Y = 510;
    public static final int STACK_INIT_Y_INC = 50;

    public static final Color INVALID_REGION_COLOR = MyRegioVincoDataModel.makeColor(220, 110, 110);

    public static final int SUB_STACK_VELOCITY = 2;
    public static final double SUB_STACK_ACCEL = .25;
    public static final int FIRST_REGION_Y_IN_STACK = GAME_HEIGHT - 140;

    public static final String AUDIO_DIR = "./data/audio/";
    public static final String SUCCESS_FILE_NAME = AUDIO_DIR + "Success.wav";
    public static final String FAILURE_FILE_NAME = AUDIO_DIR + "Failure.wav";
    public static final String TRACKED_FILE_NAME = AUDIO_DIR + "Tracked.wav";
    public static final String GENERIC_VICTORY_FILE_NAME = AUDIO_DIR + "Victory.mid";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILURE";
    public static final String VICTORY = "VICTORY";
    public static final String TRACKED_SONG = "TRACKED_SONG";
    
    public static final String CSS_FILE = "MyRegioVincoCSS.css";
    
    public static boolean NAV_MODE = true;
    public static boolean musicPressed;
    public static boolean effectsPressed;
    
    public static int playMode = 0; 
    
    public static int incorrectGuesses = 0;
    
    public static final String MAP_NOT_PLAYABLE = "Sorry!\nThis map is\ncurrently unplayable.";
    
    public static final String HELP_TEXT = "Welcome to Regio Vinco!" +
            "\n\nRegio Vinco is a geography game designed to test your knowledge"
            + "of the world, its continents, their countries, and those countries' regions."
            + "  Players will be able to choose between four modes of gameplay, whose buttons will be pure white if "
            + "available for play.\n\n"
            + "Region Mode: Denoted by the globe, this will test the player's knowledge of the regions"
            + " currently displayed on screen.\n\nCapital Mode: Denoted by the building, this will test the "
            + "player's knowledge of the capitals of the regions currently displayed on screen.\n\n"
            + "Leader Mode: Denoted by the crown, this will test player's knowledge of the leaders of the"
            + "regions currently displayed on screen.\n\nFlag Mode:  Denoted by the flag, this will test the"
            + " player's knowledge of the flags of the regions currently displayed on screen.\n\nIn all"
            + " four game modes, players will be tasked with choosing the region corresponding to what's on the bottom of the stack"
            + " displayed on the right of the screen, depending on the game mode.  Score is determined by time taken"
            + " and the number of incorrect guesses made.\n\nClick on the globe below "
            + "to return to the map.\n\nGood luck!\n - Douglas Bell";
/**
     * This is where the RegioVinco application starts. It proceeds to make a
     * game and pass it the window, and then starts it.
     *
     * @param primaryStage The window for this JavaFX application.
     */
    @Override
    public void start(Stage primaryStage) {      
	MyRegioVincoGame game = new MyRegioVincoGame(primaryStage);
	game.startGame();
    }

    /**
     * The RegioVinco game application starts here. All game data and GUI
     * initialization is done through the constructor, so we will just construct
     * our game and set it visible to start it up.
     *
     * @param args command line arguments, which will not be used
     */
    public static void main(String[] args) {
	launch(args);
    } 
}
